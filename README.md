# Ansible role for simple CA

# Role variables
```
ca_certificates:
  - cn: host.example.com
    extendedKeyUsage:
      - clientAuth
      - serverAuth
    san:
      DNS:
        - host2.example.com
        - host3.example.com
    targets:
      - cert_path: /etc/foobar/cert.pem # required
        key_path: /etc/foobar/key.pem # required
        ca_path: /etc/foobar/ca.pem # required
        reload_cmd: "rc-service foobar restart" # required
        user: foo # optional
        group: bar # optional
```
